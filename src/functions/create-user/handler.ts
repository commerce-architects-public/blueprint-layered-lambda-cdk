import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { createUser } from '/opt/nodejs/services/user-service';
import { CreateUserResponse, User } from '/opt/nodejs/types/types';

export const handler = async (
  event: APIGatewayProxyEvent
): Promise<APIGatewayProxyResult> => {
  console.log(`received event: ${event.requestContext.requestId}`);
  if (event.body === '' || event.body === undefined) {
    throw new Error('Missing Request Body');
  }
  const createdUser: CreateUserResponse = await createUser(
    event.body as unknown as User
  );
  console.log(`created user: ${JSON.stringify(createdUser)}`);
  return {
    headers: {
      'Content-Type': 'application/json'
    },
    statusCode: 200,
    body: JSON.stringify(createdUser)
  } as APIGatewayProxyResult;
};
export default handler;
