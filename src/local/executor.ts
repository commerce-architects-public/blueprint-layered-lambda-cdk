import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';

(async () => {
  try {
    const handlerName = process.argv[2];
    const eventName = process.argv[3];
    const { apiGatewayEvent } = await import(`../events/${eventName}`);
    const { handler } = await import(handlerName);
    const resp: APIGatewayProxyResult = await handler(
      apiGatewayEvent as unknown as APIGatewayProxyEvent
    );
    console.log(resp);
  } catch (e) {
    console.log(e);
  }
})();
