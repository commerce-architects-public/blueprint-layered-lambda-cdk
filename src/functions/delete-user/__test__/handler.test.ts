import { handler } from '../handler';
import { apiGatewayEvent } from '../../../events/get-user';
import { emptyResponse } from './mocks/delete-user-mocks';
import { APIGatewayProxyEvent } from 'aws-lambda';
describe('delete-user handler', () => {
  describe('when delete-user handler is called', () => {
    it('it should delete user by id and return 200 with empty body', async () => {
      const response = await handler(
        apiGatewayEvent as unknown as APIGatewayProxyEvent
      );
      expect(response).toStrictEqual({
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 200,
        body: JSON.stringify(emptyResponse)
      });
    });
    it('it should throw an error if query param is missing', async () => {
      apiGatewayEvent.queryStringParameters.id = '';
      await expect(
        handler(apiGatewayEvent as unknown as APIGatewayProxyEvent)
      ).rejects.toThrow(/Missing Required Param/);
    });
  });
});
