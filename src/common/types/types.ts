export interface Geo {
  lat: number;
  lng: number;
}

export interface Company {
  name: string;
  catchPhrase: string;
  bs: string;
}
export interface Address {
  street: string;
  suite?: string;
  city: string;
  zipcode: string;
  geo: Geo;
  phone: string;
  website: string;
}
export interface User {
  id: number;
  username: string;
  email: string;
  address: Address;
  company: Company;
}

export interface CreateUserResponse {
  id: number;
}
