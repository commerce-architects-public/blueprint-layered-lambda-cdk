#!/usr/bin/env bash

echo "Bundling ${CODE_PATH} to asset-output"

#Create an output directory for the node modules and copy them
mkdir -p /asset-output/nodejs
cp -pr ${CODE_PATH} /asset-output/nodejs/
